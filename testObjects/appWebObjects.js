
module.exports = {

    // Below section has web elements of Login page of HLI
    Login_Page: {
        link_HLILogo: {
            selector: '//img[@class="header__logo"]',
            locateStrategy: 'xpath'
        },
        input_Email: {
            selector: '//input[@type="email"]',
            locateStrategy: 'xpath'
        },
        input_Password: {
            selector: '//input[@type="password"]',
            locateStrategy: 'xpath'
        },
        btn_Submit: {
            selector: '//button[@type="submit"]',
            locateStrategy: 'xpath'
        },
        lnk_Feedback: {
            selector: '//title[contains(text(),"Usabilla Feedback Button")]/../../body/img',
            locateStrategy: 'xpath'
        },
        lnk_HLILogoFooter: {
            selector: '//img[@src="/assets/images/hli_logo_footer.png"]',
            locateStrategy: 'xpath'
        },
        lnk_HLILogoFooter: {
            selector: '//p[contains(text(),"Interested in Healthy Lives Index for your business? We\'d love to hear from you."]',
            locateStrategy: 'xpath'
        },
        lnk_MercerLogo: {
            selector: '//a[@title="Find Out More"]',
            locateStrategy: 'xpath'
        },
        lbl_WhatIsHLI: {
            selector: '//div[text()="HLI is an online service that brings together information about your health, wealth and career in one secure place, giving you get a clearer view of your present and helping you understand what changes you could make to improve your future."]',
            locateStrategy: 'xpath'
        },
        lbl_WhoIsMercer: {
            selector: '//div[text()="Mercer makes a difference in the lives of more than 110 million people every day by advancing their health, wealth, and careers. For more than 70 years, we\'ve turned our insights into actions, enabling people around the globe to live, work, and retire well."]',
            locateStrategy: 'xpath'
        },
        lbl_ValueOfHLI: {
            selector: '//div[text()="By using our simple online tools, you are able to see how small changes (for example to your lifestyle or your pension contribution) could have a significant impact on your future. It is then possible to take immediate action by choosing one of the personalized options provided."]',
            locateStrategy: 'xpath'
        },


    },

    // Following section has web elements of Forgot password page of HLI
    Forgot_Password_Page: {

        headerText_ForgotYour: {
            selector: '//h2[contains(text(),"Forgot Your")]',
            locateStrategy: 'xpath'
        },

        headerText_Password: {
            selector: '//h2/span[contains(text(),"Password")]',
            locateStrategy: 'xpath'
        },

        txt_FirstName: {
            selector: '//input[@placeholder="First Name"]',
            locateStrategy: 'xpath'
        },

        txt_LastName: {
            selector: '//input[@placeholder="Last Name"]',
            locateStrategy: 'xpath'
        },

        txt_EmailAddress: {
            selector: '//input[@placeholder="Email Address"]',
            locateStrategy: 'xpath'
        }


    },


    Dashboard_page: {
        icon_Menu: {
            selector: "//div[@class='header__burger u-flex align-middle']",
            locateStrategy: "xpath"
        },
        lnk_Home: {
            selector: "//span[text()='Home']",
            locateStrategy: "xpath"
        },
        lnk_Health: {
            selector: "//span[text()='Health']",
            locateStrategy: "xpath"
        },
        lnk_ProductAndServices: {
            selector: "//span[text()='Product & Services']",
            locateStrategy: "xpath"
        },
        lnk_ArticlesAndVideos: {
            selector: "//span[text()='Articles & Videos']",
            locateStrategy: "xpath"
        },
        lnk_DigitalFilingCabinet: {
            selector: "//span[text()='Digital Filing Cabinet']",
            locateStrategy: "xpath"
        },
        lnk_HelpAndContactsAsSubmenu: {
            selector: "//span[text()='Help & Contacts']",
            locateStrategy: "xpath"
        },
        lnk_profileIcon: {
            selector: "//a[@title='Profile']",
            locateStrategy: "xpath"

        },
        lnk_viewProfile: {
            selector: "//a[@title='View Profile']",
            locateStrategy: "xpath"

        },
        lnk_logout: {
            selector: "//a[@title='Log Out']",
            locateStrategy: "xpath"

        },
        lnk_acceptableUse: {
            selector: "//a[@title='Acceptable Use']",
            locateStrategy: "xpath"

        },
        lnk_CookiePolicy: {
            selector: "//a[@title='Cookie Policy']",
            locateStrategy: "xpath"

        },
        lnk_PrivacyPolicy: {
            selector: "//a[@title='Privacy Policy']",
            locateStrategy: "xpath"

        },
        lnk_TermsOfUse: {
            selector: "//a[@title='Terms of Use']",
            locateStrategy: "xpath"

        },
        lnk_HelpAndContacts: {
            selector: "//a[@title='Help & Contacts']",
            locateStrategy: "xpath"
        },

        btn_NotificationBell: {
            selector: "//i[@class='hrm-icon hrm-icon--notifications']",
            locateStrategy: "xpath"
        },
        lnk_HeaderAcceptableUse: {
            selector: "//div[@class='evo-layout--full-width evo-bg--sapphire-dark evo-flex-row--center evo-subnav hide-for-small-only']//a[contains(text(),'Acceptable Use')]",
            locateStrategy: "xpath"

        },
        lnk_HeaderCookiePolicy: {
            selector: "//div[@class='evo-layout--full-width evo-bg--sapphire-dark evo-flex-row--center evo-subnav hide-for-small-only']//a[contains(text(),'Cookie Policy')]",
            locateStrategy: "xpath"

        },
        lnk_HeaderPrivacyPolicy: {
            selector: "//div[@class='evo-layout--full-width evo-bg--sapphire-dark evo-flex-row--center evo-subnav hide-for-small-only']//a[contains(text(),'Privacy Policy')]",
            locateStrategy: "xpath"

        },
        lnk_HeaderTermsOfUse: {
            selector: "//div[@class='evo-layout--full-width evo-bg--sapphire-dark evo-flex-row--center evo-subnav hide-for-small-only']//a[contains(text(),'Terms of Use')]",
            locateStrategy: "xpath"

        },
        lnk_HeaderHelpAndContacts: {
            selector: "//div[@class='evo-layout--full-width evo-bg--sapphire-dark evo-flex-row--center evo-subnav hide-for-small-only']//a[contains(text(),'Help & Contacts')]",
            locateStrategy: "xpath"
        }

    },

    //locators for Privacy Policy dashboard
    Privacy_Policy_Dashboard: {
        header_PrivacyPolicyDashBoard: {
            selector: "//h1[text()='Privacy Policy dashboard']",
            locateStrategy: "xpath"
        },
        header_WHAT_DATA_DO_WE_COLLECT: {
            selector: "//h3[text()='1. WHAT DATA DO WE COLLECT?']",
            locateStrategy: "xpath"
        },
        header_HOW_DO_WE_USE_THE_DATA_WE_COLLECT: {
            selector: "//h3[text()='2. HOW DO WE USE THE DATA WE COLLECT?']",
            locateStrategy: "xpath"
        },
        header_WITH_WHOM_DO_WE_SHARE_YOUR_DATA: {
            selector: "//h3[text()='3. WITH WHOM DO WE SHARE YOUR DATA?']",
            locateStrategy: "xpath"
        },
        header_WHAT_STEPS_DO_WE_TAKE_TO_PROTECT_YOUR_INFORMATION: {
            selector: "//h3[text()='4. WHAT STEPS DO WE TAKE TO PROTECT YOUR INFORMATION?']",
            locateStrategy: "xpath"
        },
        header_WHAT_RIGHTS_AND_OBLIGATIONS_DO_YOU_HAVE_WITH_RESPECT_TO_YOUR_DATA: {
            selector: "//h3[text()='5. WHAT RIGHTS AND OBLIGATIONS DO YOU HAVE WITH RESPECT TO YOUR DATA?']",
            locateStrategy: "xpath"
        },
        header_SOME_OTHER_MATTERS: {
            selector: "//h3[text()='6. SOME OTHER MATTERS']",
            locateStrategy: "xpath"
        },

    },

    // Locators for cookies policy
    Cookies_Policy_Dashboard: {
        header_CookiesPolicyDashBoard: {
            selector: "//h1[text()='Cookie Policy dashboard']",
            locateStrategy: "xpath"
        },
        header_WHAT_IS_A_COOKIE: {
            selector: "//h3[text()='1. WHAT IS A COOKIE?']",
            locateStrategy: "xpath"
        },
        header_HOW_AND_WHY_WE_USE_COOKIES: {
            selector: "//h3[text()='2. HOW AND WHY WE USE COOKIES']",
            locateStrategy: "xpath"
        },
        header_KEEPING_YOUR_PERSONAL_INFORMATION_SAFE: {
            selector: "//h3[text()='3. KEEPING YOUR PERSONAL INFORMATION SAFE']",
            locateStrategy: "xpath"
        },
        header_WHAT_COOKIES_DO_WE_USE_ON_OUR_WEBSITE: {
            selector: "//h3[text()='4. WHAT COOKIES DO WE USE ON OUR WEBSITE?']",
            locateStrategy: "xpath"
        },
        header_HOW_TO_MANAGE_COOKIES_THROUGH_THE_BROWSER: {
            selector: "//h3[text()='5. HOW TO MANAGE COOKIES THROUGH THE BROWSER']",
            locateStrategy: "xpath"
        }
    },

    // Locators for terms of use page
    Terms_Of_Use_Dashboard: {
        header_TERMSOFUSEDASHBOARD: {
            selector: "//h1[text()='Terms of Use dashboard']",
            locateStrategy: "xpath"
        },
        header_General: {
            selector: "//h3[text()='1. General']",
            locateStrategy: "xpath"
        },
        header_Availibility_and_Access: {
            selector: "//h3[text()='2. Availibility and Access']",
            locateStrategy: "xpath"
        },
        header_Access_Details: {
            selector: "//h3[text()='3. Access Details']",
            locateStrategy: "xpath"
        },
        header_Information: {
            selector: "//h3[text()='4. Information']",
            locateStrategy: "xpath"
        },
        header_Your_Use: {
            selector: "//h3[text()='5. Your Use']",
            locateStrategy: "xpath"
        },
        header_Our_rights_in_this_Website: {
            selector: "//h3[text()='6. Our rights in this Website']",
            locateStrategy: "xpath"
        },
        header_Linking_from_this_Website: {
            selector: "//h3[text()='7. Linking from this Website']",
            locateStrategy: "xpath"
        },
        header_Linking_to_this_Website: {
            selector: "//h3[text()='8. Linking to this Website']",
            locateStrategy: "xpath"
        },
        header_Applicable_Law: {
            selector: "//h3[text()='9. Applicable Law']",
            locateStrategy: "xpath"
        },
        header_Changes: {
            selector: "//h3[text()='10. Changes']",
            locateStrategy: "xpath"
        },
        header_Limitation_of_Liability: {
            selector: "//h3[text()='11. Limitation of Liability']",
            locateStrategy: "xpath"
        },
        header_Contact_us: {
            selector: "//h3[text()='12. Contact us']",
            locateStrategy: "xpath"
        }


    },


    // Locators for Acceptable Use
    Terms_Of_Acceptable_Use: {
        header_Acceptable_Use: {
            selector: "//h1[text()='Acceptable Use']",
            locateStrategy: "xpath"
        },
        header_Prohibited_Uses: {
            selector: "//h3[text()='1. Prohibited uses']",
            locateStrategy: "xpath"
        },
        header_Interactive_Services: {
            selector: "//h3[text()='2. Interactive services']",
            locateStrategy: "xpath"
        },
        header_Content_Standards: {
            selector: "//h3[text()='3. Content standards']",
            locateStrategy: "xpath"
        },
        header_CHANGES_TO_THE_ACCEPTABLE_USE_POLICY: {
            selector: "//h3[text()='4. Changes to the acceptable use policy']",
            locateStrategy: "xpath"
        },
    },

    // Locators for Help and contact
    Terms_Of_Help_And_Contact: {
        header_Need_Help_Want_To_Give_Feedback: {
            selector: "//h5[contains(text(),'Need help or want to give feedback')]",
            locateStrategy: "xpath"
        },
        lnk_HLIMailId: {
            selector: "//div[@class='row contact-buttons show-for-large-only']//a[contains(text(),'healthylives@mercer.com')]",
            locateStrategy: "xpath"
        }
    }

}

