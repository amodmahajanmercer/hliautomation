exports.config = {
    baseUrl: 'http://www.google.co.in/',
    projectName: 'Harmonise AgeingWorks',
    isApplicationAngular : true,
    // Specify the patterns for test files
    // to include in the test run
    specs: [
        'features/**/*.feature'
    ],

    allScriptsTimeout: 240000, //This is the overall Timeout
    getPageTimeout: 60000, //This is the Page timeout
    directConnect: true,
    ignoreUncaughtExceptions: true, // This is to execute next step/scenario after the assertion failure
    // Use this to exclude files from the test run.
    // In this case it's empty since we want all files
    // that are mentioned in the specs.
    //SELENIUM_PROMISE_MANAGER: 1,
    //restartBrowserBetweenTests: true,
    exclude: [],
    // Use cucumber for the tests
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    // Contains additional settings for cucumber-js
    cucumberOpts: {
        format: ['json:reports/json/results.json', 'pretty'],
        require: ['features/step_definitions/*.js','support/*.js' ],
        tags: "@HLI-101 or @HLI-42"

    },

    // These capabilities tell protractor about the browser
    // it should use for the tests. In this case chrome.
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            'args': ['--disable-extensions','--start-maximized'],
        }
    },

    // This setting tells protractor to wait for all apps
    // to load on the page instead of just the first.
    onPrepare: () => {
        global.globalData = require('./testData/global.js');
        if(globalData.isApplicationAngular){
            console.log('~Testing a Angular Application~');
            //browser.waitForAngular();
            browser.ignoreSynchronization = false;
        }else{
            browser.ignoreSynchronization = true;
            console.log('~Testing a Non-Angular Application~');
        }



        var chai = require('chai');
        var chaiAsPromised = require('chai-as-promised');
        chai.use(chaiAsPromised);
        global.expect = chai.expect;

        var { defineSupportCode } = require("cucumber");
        defineSupportCode(function ({ setDefaultTimeout }) {
            setDefaultTimeout(30000);
        });

        global.keyword = require('./utilities/keywords.js');
    },

    beforeLaunch: () => {

    },

    afterLaunch: () => {

    },

    onComplete: () => {
    },

    onCleanUp: () => {
    },

}