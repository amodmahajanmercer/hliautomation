/**
 * Created by amod-mahajan on 11/15/2017.
 */

var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');

defineSupportCode(function ({Given}) {

    Given(/^User access HLI url$/, function (callback) {
        browser.get(globalData.AUTURLs[globalData.Environment]);
        browser.waitForAngularEnabled(false);
        callback();
    });

        Given(/^Verify correct HLI home page is loaded successfully$/, function (callback) {
            keyword.expectVisible('Login_Page|link_HLIlogo', function () {
                callback();
            })
        });

        Given(/^User verify presence of Email text box and enters email id as "([^"]*)"$/, function (arg1, callback) {
            keyword.expectVisible('Login_Page|input_Email', function () {
                keyword.setText('Login_Page|input_Email', arg1, function () {
                    callback();
                });
            });
        });

        Given(/^User verify presence of Submit button and click on it$/, function (callback) {
            keyword.expectVisible('Login_Page|btn_Submit', function () {
                keyword.performclick('Login_Page|btn_Submit', function () {
                    callback();
                });
            });
        });

        Given(/^User navigates to forgot password page and verify page header$/, function (callback) {
            keyword.expectVisible('Forgot_Password_Page|headerText_ForgotYour', function () {
                keyword.expectVisible('Forgot_Password_Page|headerText_Password', function () {
                    callback();
                });
            });
        });

        Given(/^User verify presence of First Name text box and enters first name as "([^"]*)"$/, function (arg1, callback) {
            keyword.expectVisible('Forgot_Password_Page|txt_FirstName', function () {
                keyword.setText('Forgot_Password_Page|txt_FirstName', arg1, function () {
                    callback();
                });
            });
        });

        Given(/^User verify presence of Last Name text box and enters last name as "([^"]*)"$/, function (arg1, callback) {
            keyword.expectVisible('Forgot_Password_Page|txt_LastName', function () {
                keyword.setText('Forgot_Password_Page|txt_LastName', arg1, function () {
                    callback();
                });
            });
        });

        Given(/^User verify presence of Email Address text box and enters email address as "([^"]*)"$/, function (arg1, callback) {
            keyword.expectVisible('Forgot_Password_Page|txt_EmailAddress', function () {
                keyword.setText('Forgot_Password_Page|txt_EmailAddress', arg1, function () {
                    callback();
                });
            });
        });

        Given(/^User clicks on expired (.*)$/, function (arg1, callback) {
            browser.get(arg1);
            callback();
        });

        Given(/^User should be directed to a page with title YOUR PASSWORD RESET LINK HAS EXPIRED$/, function (callback) {
            keyword.getAndVerifyPageTitle('YOUR PASSWORD RESET LINK HAS EXPIRED', function () {
                callback();
            });
        });
        Given(/^User verify error message as Your password reset link has expired\. Please follow the instructions below$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            });
        });
        Given(/^User verify presence of contact our support team link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            });
        });
        Given(/^User verify presence of Back to Login button$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            });
        });
        Given(/^User clicks on contact our support team link$/, function (callback) {
            keyword.performclick('', function () {
                callback();
            })
        });
        Given(/^User navigates to Help & Support page$/, function (callback) {
            keyword.getAndVerifyPageTitle('', function () {
                callback();
            })
        });

        Given(/^User go back to previous page$/, function (callback) {
            browser.back();
            callback();
        });

        Given(/^User clicks on Login button$/, function (callback) {
            keyword.expectVisible('', function () {
                keyword.performclick('', function () {
                    callback();
                });
            });
        });

        Given(/^User navigates to Login page page$/, function (callback) {
            keyword.getAndVerifyPageTitle('', function () {
                callback();
            })
        });
});


