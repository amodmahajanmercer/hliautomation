/**
 * Created by amod-mahajan on 11/17/2017.
 */

var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');


defineSupportCode(function ({Given}) {
    Given(/^the system should direct him to the Congratulations page$/, function (callback) {
        keyword.expectVisible('',function () {
            callback();
        })
    });

    Given(/^page should include "([^"]*)" button$/, function (arg1, callback) {
        keyword.expectVisible('',function () {
            callback();
        })
    });
    Given(/^User clicks "([^"]*)" button$/, function (arg1, callback) {
        keyword.performclick('',function () {
            callback();
        })
    });
});
