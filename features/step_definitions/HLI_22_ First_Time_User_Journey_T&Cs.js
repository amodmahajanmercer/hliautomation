/**
 * Created by amod-mahajan on 11/14/2017.
 */


var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');


defineSupportCode(function ({Given}) {

    Given(/^User should see Terms & Conditions \(T&C\) page of HLI as first\-time user$/, function (callback) {
        keyword.getAndVerifyPageTitle('',function () {
            callback();
        })
    }),
        Given(/^T&C page includes a page header$/, function (callback) {
            keyword.expectVisible('',function () {
                callback();
            })
        }),
        Given(/^T&C page shows Full policy$/, function (callback) {
            keyword.expectVisible('',function () {
                callback();
            })
        }),
        Given(/^T&C page includes a checkbox to accept the T&C$/, function (callback) {
            keyword.expectVisible('',function () {
                callback();
            })
        }),
        Given(/^T&C page includes a button to Agree to accepted T&C$/, function (callback) {
            keyword.expectVisible('',function () {
                callback();
            })
        }),
        Given(/^T&C page includes an option to Disagree to T&C$/, function (callback) {
            keyword.expectVisible('',function () {
                callback();
            })
        }),
        Given(/^User does not check in Accept T&C checkbox$/, function (callback) {
            callback();
        }),
        Given(/^Agree button should be in disabled state by default$/, function (callback) {
            callback();
        }),
        Given(/^User should receive tool tip on clicking of disabled Agree button$/, function (callback) {
            callback();
        }),
        Given(/^Disagree CTA link should be active by default$/, function (callback) {
            callback();
        }),
        Given(/^User checks in Accept T&C checkbox$/, function (callback) {
            keyword.expectVisible('',function () {
                keyword.performclick('',function () {
                    callback();
                })
            })
        }),
        Given(/^Agree button should change to enabled state$/, function (callback) {
            callback();
        }),
        Given(/^User clicks on Agree button to continue welcome journey$/, function (callback) {
            keyword.expectVisible('',function () {
                keyword.performclick('',function () {
                    callback();
                })
            })
        }),
        Given(/^email message to welcome user in HLI should be triggered$/, function (callback) {
            callback();
        }),
        Given(/^User clicks on Disagree to deny T&C$/, function (callback) {
            keyword.expectVisible('',function () {
                keyword.performclick('',function () {
                    callback();
                })
            })
        }),
        Given(/^System should launch browser modal with copy$/, function (callback) {
            callback();
        }),
        Given(/^modal should include dismiss button to close modal$/, function (callback) {
            callback();
        });
});
