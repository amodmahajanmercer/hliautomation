var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var UserDateOfBirth = "";

defineSupportCode(function ({Given}) {

    Given(/^User has logged in HLI for the first time$/, function (done) {
        console.log("Launching URL....")
        browser.get("http://usdf23v0218.mrshmc.com:5024/provision/login");
        browser.waitForAngularEnabled(false);
        keyword.expectVisible('Login_Page|input_Email', function () {
            console.log("Checking visibility of email text box....")
            keyword.setText('Login_Page|input_Email', HLIData.Login_Credentails.Email_Address, function () {
                console.log("Typing email address");
                keyword.expectVisible('Login_Page|btn_Submit', function () {
                    keyword.performclick('Login_Page|btn_Submit', function () {
                        keyword.expectVisible('Login_Page|input_Password', function () {
                            keyword.setText('Login_Page|input_Password', HLIData.Login_Credentails.Password, function () {
                                keyword.performclick('Login_Page|btn_Submit', function () {
                                    keyword.expectVisible('Home_Page|lbl_Ageingworks', function () {
                                        console.log("Login is successful.")
                                        done();
                                    })

                                });
                            });
                        });
                    });
                });
            });
        });
    }),

        Given(/^User is on Welcome video page$/, function (done) {
            keyword.getAndVerifyPageTitle('', function () {
                console.log("User is on welcome video page");
                done();
            })
        }),

        Given(/^User clicks on Skip Video button on Welcome page$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.performclick('', function () {
                    done();

                })
            })
        }),

        Given(/^System will populate a personal details page$/, function (done) {
            keyword.getAndVerifyPageTitle('', function () {
                console.log("User is on personal details page");
                done();
            })
        }),
        Given(/^User views and verify non-editable fields populated from HR file under About you section of page$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.getTextAndVerify('', '', function () {
                    keyword.expectVisible('', function () {
                        keyword.getTextAndVerify('', '', function () {
                            keyword.expectVisible('', function () {
                                keyword.getTextAndVerify('', '', function () {
                                    done();
                                })
                            })
                        })
                    })
                })
            })
        }),

        Given(/^User can access a link to understand How to manage changes to non-editable fields$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.getTextAndVerify('', '', function () {
                    keyword.performclick('', function () {
                        keyword.getTextAndVerify('', '', function () {
                            done();
                        });
                    });
                });
            })

        }),
        Given(/^User should be able to view and update Relationship Status under Need more information section of page$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.selectByVisibleText('', '', function () {
                    keyword.getSelectedOptionFromSelectDropdown('', function () {
                        done();
                    })
                })
            });
        }),
        Given(/^User should be able to update Rent\/Own status of Home under Need more information section of page$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.selectByVisibleText('', '', function () {
                    keyword.getSelectedOptionFromSelectDropdown('', function () {
                        done();
                    })
                })
            });
        }),
        Given(/^the User should have ability to update contact details and contact preferences$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.setText('', '', function () {
                    keyword.expectVisible('', function () {
                        keyword.selectByVisibleText('', '', function () {
                            keyword.getSelectedOptionFromSelectDropdown('', function () {
                                keyword.expectVisible('', function () {
                                    keyword.setText('', '', function () {
                                        keyword.expectVisible('', function () {
                                            keyword.selectByVisibleText('', '', function () {
                                                keyword.getSelectedOptionFromSelectDropdown('', function () {
                                                    done();
                                                })
                                            })
                                        });
                                    })
                                });
                            })
                        })
                    });
                })
            });
        }),

        Given(/^User should see a button to Finish the Welcome journey and go to HLI Dashboard$/, function (done) {
            keyword.performclick('', function () {
                done();
            })
        }),

        // Incomplete step definition
        Given(/^User has Date of Birth value available in Runpath database$/, function (callback) {

            var client_id = globalData.clientID[globalData.TestingEnvironment];
            //var runpath_url = "https://mercerstate.azurewebsites.net/?partyId="+client_id+"&environment=QA&Version=Version2";
            var runpath_url = "https://usdf23v0218.mrshmc.com:10471/api/provision/login/callback";


            var getValuesFromWebService = function (urlType, url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.open(urlType, url, false);
                xhr.send();
                var objJson = xhr.responseText;
                //console.log("string  "+objJson);
                var JSONObject = JSON.parse(objJson);
                return callback(objJson);
            };
            getValuesFromWebService('POST', runpath_url, function (object) {
                console.log(JSON.stringify(object));
            })


        }),
        Given(/^User accesses First\/ Main Accordion page$/, function (done) {
            done();
        }),
        Given(/^System should include Age as a calculated field from Date of Birth in About you section of page$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.getTextAndVerify('', '', function () {
                    done();
                })
            })
        }),
        Given(/^Age should be a non\-editable field$/, function (done) {
            done();
        }),
        Given(/^User's Date of Birth value is not available in Runpath database$/, function (done) {
            done();
        }),
        Given(/^System will not include Age field in About you section of page$/, function (done) {
            keyword.expectNotVisible('', function () {
                done();
            })
        }),
        Given(/^User clicks on Are these details correct\? link below non\-editable fields$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.getTextAndVerify('', '', function () {
                    keyword.performclick('', function () {
                        done();
                    });
                });
            })
        }),
        Given(/^System will expand text section to explain how non\-editable fields can be corrected$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.getTextAndVerify('', '', function () {
                    done();
                });
            })
        }),
        Given(/^"Are these details correct\?" link wil not be visible when text section is expanded$/, function (done) {
            keyword.expectNotVisible('', function () {
                done();
            })
        }),
        Given(/^a chevron to close the expanded text section$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.performclick('', function () {
                    done();
                });
            })
        }),
        Given(/^User clicks on "([^"]*)" button on page$/, function (done) {
            keyword.expectVisible('', function () {
                keyword.performclick('', function () {
                    done();
                });
            })
        }),
        Given(/^System should navigate User to HLI Dashboard$/, function (done) {
            keyword.getAndVerifyPageTitle('', function () {
                done();
            })
        });

});

