/**
 * Created by amod-mahajan on 11/13/2017.
 */

var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');

defineSupportCode(function ({Given}) {

    Given(/^I click the top left burger menu icon$/, function (callback) {
        keyword.expectVisible('', function () {
            keyword.performclick('', function () {
                callback();
            })
        })
    }),
        Given(/^the task bar shows Home link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),

        Given(/^the task bar shows Health Age link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),

        Given(/^the task bar shows Challenges & Badges link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),

        Given(/^the task bar shows Products & Services link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),

        Given(/^the task bar shows Articles & Videos link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),
        Given(/^the task bar shows Digital Filing Cabinet link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),
        Given(/^the task bar shows Help & Contacts link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),
        Given(/^the task bar shows Admin link$/, function (callback) {
            keyword.expectVisible('', function () {
                callback();
            })
        }),
        Given(/^I click a "([^"]*)" from the site menu$/, function (arg1, callback) {
            Keyword.expectVisible(arg1, function () {
                keyword.performclick(arg1, function () {
                    callback();
                })
            })
        }),
        Given(/^I should be directed to the "([^"]*)" link$/, function (arg1, callback) {
            keyword.getAndVerifyPageTitle(arg1, function () {
                callback();
            })
        });
});

