/**
 * Created by amod-mahajan on 11/16/2017.
 */




var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');


defineSupportCode(function ({Given}) {
    Given(/^the system should direct him to the Congratulations\/Skip page$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });

    Given(/^page should include text explaining value of continuing with the optional survey screens$/, function (callback) {

    });

    Given(/^page should include Continue Survey button$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        });
    });
    Given(/^page should include Go to Dashboard button$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        });
    });
    Given(/^User clicks Continue Survey button$/, function (callback) {
        keyword.performclick('', function () {
            callback();
        })
    });

    Given(/^User should be navigated to the HLI Blood Pressure Optional Survey screen$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });

    Given(/^User clicks Go to Dashboard button$/, function (callback) {
        keyword.performclick('', function () {
            callback();
        })
    });
    Given(/^User should be navigated to the HLI main Dashboard$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });

});
