/**
 * Created by amod-mahajan on 11/17/2017.
 */

var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');


defineSupportCode(function ({Given}) {
    Given(/^system loads the branded HLI Login page in standard design$/, function (callback) {
        keyword.expectVisible('Login_Page|link_HLILogo', function () {

            callback();


        })
    });
    Given(/^User sees HLI logo$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });

    Given(/^User sees Email address input box$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });
    Given(/^User sees next button$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });
    Given(/^User sees Feedback button$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });
    Given(/^User sees about Who Is Mercer$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });
    Given(/^User sees about What Is HLI$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });
    Given(/^User sees about The value of HLI$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });
    Given(/^User sees Healthy Living Index header and text$/, function (callback) {
        keyword.expectVisible('', function () {
            callback();
        })
    });
    Given(/^User sees Find Out More button$/, function (callback) {
        callback.pending();
    });
    Given(/^User sees Mercer logo and tag line$/, function (callback) {
        callback.pending();
    });
    Given(/^User enters email id and click on next button$/, function (callback) {
        callback.pending();
    });
    Given(/^User types password click on Next$/, function (callback) {
        callback.pending();
    });
    Given(/^System should authorise access for registered users$/, function (callback) {
        callback.pending();
    });
    Given(/^User sees Find Out More button and clicks on it$/, function (callback) {
        callback.pending();
    });
    Given(/^User should be redirected to user to the HLI B2B contacts page$/, function (callback) {
        callback.pending();
    });
    Given(/^System should not authorise access for non\-registered users$/, function (callback) {
        callback.pending();
    });
});
