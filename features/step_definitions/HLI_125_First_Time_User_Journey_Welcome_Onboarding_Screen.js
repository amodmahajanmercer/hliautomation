/**
 * Created by amod-mahajan on 11/16/2017.
 */
var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');
var HLIData = require('../../testdata/HLIData.js');


defineSupportCode(function ({Given}) {

    Given(/^User has logged in HLI for the first time using username as "([^"]*)" and password as "([^"]*)"$/, function (arg1, arg2, callback) {
        keyword.expectVisible('Login_Page|input_Email', function () {
            keyword.setText('Login_Page|input_Email', arg1, function () {
                keyword.expectVisible('Login_Page|btn_Submit', function () {
                    keyword.performclick('Login_Page|btn_Submit', function () {
                        keyword.expectVisible("Login_Page|input_Password", function () {
                            keyword.setText('Login_Page|input_Password', arg2, function () {
                                keyword.performclick('Login_Page|btn_Submit', function () {
                                    keyword.expectVisible('Home_Page|link_HLIlogo', function () {
                                        console.log("Login is successful.")
                                        callback();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    Given(/^the system should direct him to the Welcome page$/, function (callback) {
        keyword.getAndVerifyPageTitle('',function () {
            callback();
        });
    });

    Given(/^User can view the points of value of the Health Module$/, function (callback) {
        keyword.expectVisible('',function () {
            callback();
        });
    });

    Given(/^User can view and click  the Accept & Activate button$/, function (callback) {
        keyword.expectVisible('',function () {
            keyword.performclick('',function () {
                callback();
            })
        });
    });

    Given(/^User can view and click on the link for Connect An App to connect an App to the Health Module$/, function (arg1, callback) {
        keyword.expectVisible('',function () {
            keyword.performclick('',function () {
                callback();
            })
        });
    });

    Given(/^User can view and click on the link for  "([^"]*)" to enter the Health On boarding Module$/, function (arg1, callback) {
        keyword.expectVisible('',function () {
            keyword.performclick('',function () {
                callback();
            })
        });
    });

    Given(/^if the User clicks on the link for "([^"]*)" on the Health panel of the Dashboard$/, function (arg1, callback) {
        keyword.expectVisible('',function () {
            keyword.performclick('',function () {
                callback();
            })
        });
    });

    Given(/^the system should direct him to the introduction page$/, function (callback) {
        keyword.getAndVerifyPageTitle('',function () {
            callback();
        })
    });

});
