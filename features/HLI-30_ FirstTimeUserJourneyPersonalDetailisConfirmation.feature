Feature: HLI-30_ FirstTimeUserJourneyPersonalDetailsConfirmation
  As a first time User I want to view and update my basic details so that I can use functionalities in HLI platform
  in an informed manner

  @test999
  Scenario: User accesses First time
    Given User has logged in HLI for the first time
    And User is on Welcome video page
    When User clicks on Skip Video button on Welcome page
    Then System will populate a personal details page
    And User views and verify non-editable fields populated from HR file under About you section of page
    And User can access a link to understand How to manage changes to non-editable fields
    And User should be able to view and update Relationship Status under Need more information section of page
    And User should be able to update Rent/Own status of Home under Need more information section of page
    And the User should have ability to update contact details and contact preferences
    And User should see a button to Finish the Welcome journey and go to HLI Dashboard


  @test991
  Scenario: User Date of Birth is available in database
    Given User has Date of Birth value available in Runpath database
    When User accesses First/ Main Accordion page
    Then System should include Age as a calculated field from Date of Birth in About you section of page
    And Age should be a non-editable field

  @test991
  Scenario: User Date of Birth is not available in database
  Given User's Date of Birth value is not available in Runpath database
  When User accesses First/ Main Accordion page
  And System will not include Age field in About you section of page

  @test991
  Scenario: User clicks on link on How to manage changes to non-editable fields
  Given User accesses First/ Main Accordion page
  When User clicks on Are these details correct? link below non-editable fields
  Then System will expand text section to explain how non-editable fields can be corrected
  And "Are these details correct?" link wil not be visible when text section is expanded
  And a chevron to close the expanded text section

  @test991
  Scenario: User clicks on "Finish, Take me to Dashboard"
  Given User accesses First/ Main Accordion page
  When User clicks on "Finish, Take me to Dashboard" button on page
  Then System should navigate User to HLI Dashboard
