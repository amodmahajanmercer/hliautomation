Feature: HLI_132_FirstTime_User_Jjourney_ Skip_Screen
  As a New User I want to view a SKIP screen so that I can choose whether to continue with the optional onboarding screens or skip straight to the dashboard


  @test
  Scenario Outline: User clicks on "Continue Survey" on Congratulations/Skip page
    Given User access HLI url
    And User has logged in HLI for the first time using username as "<UserName>" and password as "<Password>"
    Given User has finished all mandatory onboarding survey screens
    Then the system should direct him to the Congratulations/Skip page
    And page should include text explaining value of continuing with the optional survey screens
    And page should include Continue Survey button
    And page should include Go to Dashboard button
    When User clicks Continue Survey button
    Then User should be navigated to the HLI Blood Pressure Optional Survey screen
    Examples:
      | UserName | Password |

  @test
  Scenario Outline: User clicks on "Go to Dashboard" on Congratulations/Skip page
    Given User access HLI url
    And User has logged in HLI for the first time using username as "<UserName>" and password as "<Password>"
    Given User has finished all mandatory onboarding survey screens
    Then the system should direct him to the Congratulations/Skip page
    And page should include text explaining value of continuing with the optional survey screens
    And page should include Continue Survey button
    And page should include Go to Dashboard button
    When User clicks Go to Dashboard button
    Then User should be navigated to the HLI main Dashboard
    Examples:
      | UserName | Password |


