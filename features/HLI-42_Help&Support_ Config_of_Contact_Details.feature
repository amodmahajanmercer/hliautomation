Feature: HLI-42_Help&Support_ Config_of_Contact_Details
  As a HLI user I want see the Help & Support Contact details relevant to my location & employer so that I can reach out to
  them for help

  Background:
    Given User has logged in HLI

  @HLI-42
  Scenario: User should see HLI support email address in Help & Contact page.

    When User accesses dashboard page
    Then User clicks on HLI Help & Contacts link
    When System loads Help & Contacts page in new tab and user switches to it
    Then User should see HLI support email address as "healthylives@mercer.com"
