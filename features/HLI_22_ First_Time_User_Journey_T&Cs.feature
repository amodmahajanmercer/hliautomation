Feature: HLI_22_ First_Time_User_Journey_T&Cs
  As a first time user I would like to understand Terms & Conditions (T&C) that apply to my use of and any information I may obtain from HLI

  @test
  Scenario: Federated SSO user logs first time in HLI
    Given Federated SSO User has been provisioned in HLI
    When User clicks on activation link in activation email
    Then User should see Terms & Conditions (T&C) page of HLI as first-time user
    And T&C page includes a page header
    And T&C page shows Full policy
    And T&C page includes a checkbox to accept the T&C
    And T&C page includes a button to Agree to accepted T&C
    And T&C page includes an option to Disagree to T&C

  @test
  Scenario: Non-SSO User logs first time in HLI
    Given Non-SSO User has been provisioned in HLI
    And User has completed Registration page
    When User has logged in HLI
    Then User should see Terms & Conditions (T&C) page of HLI as first-time user
    And T&C page includes a page header
    And T&C page shows Full policy
    And T&C page includes a checkbox to accept the T&C
    And T&C page includes a button to Agree to accepted T&C
    And T&C page includes an option to Disagree to T&C

  @test
  Scenario: Default state of Agree & Disagree button
    Given Non-SSO User has been provisioned in HLI
    And User has completed Registration page
    When User has logged in HLI
    Then User should see Terms & Conditions (T&C) page of HLI as first-time user
    When User does not check in Accept T&C checkbox
    Then Agree button should be in disabled state by default
    And User should receive tool tip on clicking of disabled Agree button
    And Disagree CTA link should be active by default

  @test
  Scenario: User clicks on Accept T&C checkbox
    Given Non-SSO User has been provisioned in HLI
    And User has completed Registration page
    When User has logged in HLI
    Then User should see Terms & Conditions (T&C) page of HLI as first-time user
    When User checks in Accept T&C checkbox
    Then Agree button should change to enabled state
    And Disagree CTA link should be active by default

  @test
  Scenario: User accepts T&C to Agree
    Given Non-SSO User has been provisioned in HLI
    And User has completed Registration page
    When User has logged in HLI
    Then User should see Terms & Conditions (T&C) page of HLI as first-time user
    When User checks in Accept T&C checkbox
    When User clicks on Agree button to continue welcome journey
    Then email message to welcome user in HLI should be triggered

  @test
  Scenario: User clicks Disagree
    Given Non-SSO User has been provisioned in HLI
    And User has completed Registration page
    When User has logged in HLI
    Then User should see Terms & Conditions (T&C) page of HLI as first-time user
    When User clicks on Disagree to deny T&C
    Then System should launch browser modal with copy
    And modal should include dismiss button to close modal