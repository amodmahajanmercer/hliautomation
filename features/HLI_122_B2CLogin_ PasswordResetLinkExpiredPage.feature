Feature: HLI_122_B2CLogin_ PasswordResetLinkExpiredPage
  As a User I would like to view an informative page in case the password reset link provided to me has expired


  @test
  Scenario Outline: Validity period for password reset link
    Given User access HLI url
    Then Verify correct HLI home page is loaded successfully
    Then User verify presence of Email text box and enters email id as "<Email Id>"
    Then User verify presence of Submit button and click on it
    When User navigates to forgot password page and verify page header
    Then User verify presence of First Name text box and enters first name as "<First Name>"
    And  User verify presence of Last Name text box and enters last name as "<Last Name>"
    And  User verify presence of Email Address text box and enters email address as "<Email Address>"
    Examples:
      | Email Id | First Name | Last Name | Email Address |


  @test
  Scenario Outline: Password expired page on clicking expired link
    Given User clicks on expired <"password reset link">
    Then User should be directed to a page with title YOUR PASSWORD RESET LINK HAS EXPIRED
    And User verify error message as Your password reset link has expired. Please follow the instructions below
    And User verify presence of contact our support team link
    When User clicks on contact our support team link
    Then User navigates to Help & Support page
    Then User go back to previous page
    And User verify presence of Back to Login button
    When User clicks on Login button
    Then User navigates to Login page page
    Examples:
      | "password reset link" |