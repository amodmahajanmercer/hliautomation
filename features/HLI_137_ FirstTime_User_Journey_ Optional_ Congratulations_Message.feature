Feature: HLI_137_ FirstTime_User_Journey_ Optional_ Congratulations_Message

  As a User I want to view a Congratulations message at the end of the optional onboarding journey in case when I complete it for the first time

  Scenario Outline: User completes optional Health survey screens
    Given User access HLI url
    And User has logged in HLI for the first time using username as "<UserName>" and password as "<Password>"
    When User has finished all optional onboarding survey screens
    Then the system should direct him to the Congratulations page
    And page should include "Go to Dashboard" button
    When User clicks "Go to Dashboard" button
    Then User should be navigated to the HLI main Dashboard
    Examples:
      | UserName | Password |