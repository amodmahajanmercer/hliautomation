Feature: HLI-29_SiteMenu_Footers_ SiteMenuSetup.feature
  As an HLI User I would like there to be a site menu on the website so I can seamlessly navigate around the site pages

  @test
  Scenario: Burger menu displays site pages
    Given User has logged in HLI
    When I click the top left burger menu icon
    Then the task bar shows Home link
    Then the task bar shows Health Age link
    Then the task bar shows Challenges & Badges link
    Then the task bar shows Products & Services link
    Then the task bar shows Articles & Videos link
    Then the task bar shows Digital Filing Cabinet link
    Then the task bar shows Help & Contacts link
    Then the task bar shows Admin link


  @test
  Scenario Outline: Selecting a page element in the site menu
    Given User has logged in HLI
    When I click the top left burger menu icon
    Then I click a "<link>" from the site menu
    Then I should be directed to the "<page title>" link

    Examples:
      | link                   | page title                  |
      | Home                   | Dashboard                     |
      | Health Age             | Link to Health summary        |
      | Challenges & Badges    | Links to challenges & badges  |
      | Products & Services    | Link to static benefits page  |
      | Articles & Videos      | Link to articles & Video page |
      | Digital Filing Cabinet | links to DFC                  |
      | Help & Contacts        | Support page                  |
      | Admin                  | Link to Admin section         |



