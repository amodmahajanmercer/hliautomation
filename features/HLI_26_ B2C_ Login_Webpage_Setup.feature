Feature: HLI_26_ B2C_ Login_Webpage_Setup.feature

  As a HLI User I want to log in using my registered credentials from a branded Login page so that I can access the
  HLI survey and my Health score

  @test26
  Scenario: Successful login on HLI platform using valid credentials
    Given User access HLI url
    And system loads the branded HLI Login page in standard design
    And User sees HLI logo
    And User sees Email address input box
    And User sees next button
    And User sees Feedback button
    And User sees about Who Is Mercer
    And User sees about What Is HLI
    And User sees about The value of HLI
    And User sees Healthy Living Index header and text
    And User sees Find Out More button
    And User sees Mercer logo and tag line
    When User enters email id and click on next button
    Then User types password click on Next
    Then System should authorise access for registered users



  Scenario: User clicks on "Find Out More" button
    Given User access HLI url
    And system loads the branded HLI Login page in standard design
    And User sees Find Out More button and clicks on it
    Then User should be redirected to user to the HLI B2B contacts page


  Scenario: Unsuccessful login on HLI platform using valid email id and invalid password
    Given User access HLI url
    And system loads the branded HLI Login page in standard design
    When User enters email id and click on next button
    Then User types password click on Next
    Then System should not authorise access for non-registered users


  Scenario: Unsuccessful login on HLI platform using invalid email id
    Given User access HLI url
    And system loads the branded HLI Login page in standard design
    When User enters email id and click on next button
    Then System should not authorise access for non-registered users


