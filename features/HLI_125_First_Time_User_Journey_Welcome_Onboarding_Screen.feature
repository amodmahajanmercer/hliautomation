Feature: HLI_125_First_Time_User_Journey_Welcome_Onboarding_Screen
  As a New User I want to view an introduction screen so that I can receive some context for the Health Screens consisting of the points of value


  Scenario Outline: First time User on the HLI Platform clicks on  "Accept & Activate" button and click on "Connect An App"
    Given User access HLI url
    And User has logged in HLI for the first time using username as "<UserName>" and password as "<Password>"
    And User has completed the Personal details page of the 1st time onboarding journey
    Then the system should direct him to the Welcome page
    And User can view the points of value of the Health Module
    And User can view and click  the Accept & Activate button
    And User can view and click on the link for Connect An App to connect an App to the Health Module
    Examples:
      | UserName | Password |


  Scenario Outline: First time User on the HLI Platform clicks on  "Accept & Activate" button and click on " "Continue Manually""
    Given User access HLI url
    And User has logged in HLI for the first time using username as "<UserName>" and password as "<Password>"
    And User has completed the Personal details page of the 1st time onboarding journey
    Then the system should direct him to the Welcome page
    And User can view the points of value of the Health Module
    And User can view and click  the Accept & Activate button
    And User can view and click on the link for  "Continue Manually" to enter the Health On boarding Module
    Examples:
      | UserName | Password |


  Scenario Outline: A returning user on the HLI Platform enters mhealth module without entering sufficient data to calculate his Health Age in the previous sessions
    Given User access HLI url
    And User has logged in HLI for the first time using username as "<UserName>" and password as "<Password>"
    And the User had not entered sufficient data to calculate his Health Age in the previous sessions
    And if the User clicks on the link for "Get Started" on the Health panel of the Dashboard
    Then the system should direct him to the introduction page
    And User can view the points of value of the Health Module
    And User can view and click on the link for Connect An App to connect an App to the Health Module
    Examples:
      | UserName | Password |


  Scenario Outline: A returning user on the HLI Platform enters mhealth module without entering sufficient data to calculate his Health Age in the previous sessions
    Given User access HLI url
    And User has logged in HLI for the first time using username as "<UserName>" and password as "<Password>"
    And the User had not entered sufficient data to calculate his Health Age in the previous sessions
    And if the User clicks on Health in the burger menu
    Then the system should direct him to the introduction page
    And User can view the points of value of the Health Module
    And User can view and click on the link for  "Continue Manually" to enter the Health On boarding Module
    Examples:
      | UserName | Password |
